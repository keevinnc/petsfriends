
@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">LISTADO</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <ul>
                        <li><a href="{{ action('HomeController@pet','1') }}">Uno</a></li>
                        <li><a href="{{ action('HomeController@pet','2') }}">Dos</a></li>
                        <li><a href="{{ action('HomeController@pet','3') }}">Tres</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
