
@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row">
                <div class="col-md-8 col-md-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading"><u><a href="{{ action('HomeController@index') }}">Volver</a></u></div>
                        </div>
                </div>
        </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">LISTADO</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <ul>
                        <li><strong>Name: </strong>{{ $pet['name'] }}</li>
                        <li><strong>Age: </strong> {{ $pet['age'] }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
