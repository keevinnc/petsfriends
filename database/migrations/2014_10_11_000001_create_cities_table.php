<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            Schema::dropIfExists('cities');
            $table->bigIncrements('id');
            // $table->bigInteger('id_province');
            $table->string('name');
        });
/*         Schema::table('cities', function($table)	
        {
            $table->foreign('id_province')	
                ->references('id')->on('provinces')		
                ->onDelete('cascade');																				
        }); */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
