<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            Schema::dropIfExists('users');
            $table->bigIncrements('id');
            $table->bigInteger('id_city')->unsigned();
            $table->string('name');
            $table->string('surname');
            $table->string('email')->unique();
            $table->integer('phone_1');
            $table->integer('phone_2')->nullable();
            $table->boolean('alerts')->nullable()->default(false);
            $table->string('avatar')->nullable();
            $table->string('social_network', 100);
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::table('users', function($table)	
        {
            $table->foreign('id_city')	
                ->references('id')->on('cities')		
                ->onDelete('cascade');																				
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
