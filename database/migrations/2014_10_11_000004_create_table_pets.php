<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pets', function (Blueprint $table) {
            Schema::dropIfExists('pets');
            $table->bigIncrements('id');
            $table->bigInteger('id_user')->unsigned();
            $table->string('name');
            $table->string('size');
            $table->string('breed');            
            $table->date('birthday')->nullable();
            $table->string('description');
            $table->timestamps();
        });
        Schema::table('pets', function($table)	
        {
            $table->foreign('id_user')	
                ->references('id')->on('users')		
                ->onDelete('cascade');							
    																		
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pets');
    }
}
