<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//public
Route::get('/', function () {
    return view('welcome');
});

//private 
Route::group(['prefix' => '/home'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/list_pets', 'HomeController@list_pets');
    Route::get('/pet/{id}', 'HomeController@pet')->where('id', '[0-9]+');;
});


