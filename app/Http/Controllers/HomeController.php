<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');  //loggin control
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return view('home');
        return view('private/pets/list_pets');
    }
    
    
    public function list_pets(){
        return view('private/pets/list_pets');
    }


    public function pet($id)
    {
        // echo $id;
        $pet['name']="blacky";
        $pet['age']="5";

        return view('private/pets/pet_profile',['pet'=>$pet]);
    }
}
